= Installation =

  # Extract du zip file in the modules directory 
  # Enable the module in the admin settings

= Use =

In the content-type settings panel, a new field "Default text" have been added. Insert the default text. It can be of any input format type.

You can insert php code and use $form variable. It will be evaluated at the form opening and not at save.
